//
//  ViewController.swift
//  yixi
//
//  Created by yudun1989 on 8/10/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, YixiPageViewControllerDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var maskButton: UIButton!
    @IBOutlet weak var maskImage: UIImageView!
    @IBOutlet weak var categoryView: UICollectionView!

    var selectedInfo:AnyObject!
    var selectedAlbum:Dictionary<String, AnyObject>!
    var targetTalk:AnyObject?
    var talksArray:NSArray = [];
    var featuredArray:NSArray = [];
    var albumButtons:NSMutableArray = NSMutableArray()
    var albumsArray:NSArray = []{
        didSet{
            self.setCategoryHeight()
        }
    }
    
    @IBAction func categoryItemTouched(sender: AnyObject) {
        UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIViewAnimationOptions.TransitionNone, animations: {
            self.maskImage.alpha = 0;
            self.categoryView.transform = CGAffineTransformMakeTranslation(0, -200)
            }, completion: { (BOOL) -> Void in
                self.categoryView.hidden = true
                self.maskButton.hidden = true
                self.maskImage.hidden = true
                self.performSegueWithIdentifier("homeToList", sender: self)
        })
    }
    
    @IBAction func maskButtonTouched(sender: AnyObject) {
        hideCategoryButton()
    }
    
    @IBAction func categoryButtonTouched(sender: AnyObject) {
        if self.categoryView.hidden{
            showCategoryButton()
        }else {
            hideCategoryButton()
        }
    }
    
    func hideCategoryButton(){
        UIView.animateWithDuration(1.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.9, options: UIViewAnimationOptions.TransitionNone, animations: {
//                self.categoryView.transform = CGAffineTransformMakeTranslation(0, -self.view.height())
                    self.categoryView.alpha = 0;
                    self.maskImage.alpha = 0;
            },  {(Bool) -> Void in
                self.categoryView.hidden = true
                self.maskButton.hidden = true
                self.maskImage.hidden = true
        })
        
    }
    
    func showCategoryButton(){
        self.categoryView.hidden = false
        self.maskButton.hidden = false
        var image:UIImage = self.view.screenshot()
        var bluredImage:UIImage = image.applyBlurWithRadius(3.0, tintColor: UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.3), saturationDeltaFactor: 1.0, maskImage: nil)
        self.maskImage.image = bluredImage
        self.maskImage.hidden = false
        self.maskImage.alpha = 0;
        self.categoryView.alpha = 1;
        self.categoryView.transform = CGAffineTransformMakeTranslation(0, -self.view.height())
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.9, options: UIViewAnimationOptions.TransitionNone, animations: {
            self.maskImage.alpha = 1;
            self.categoryView.transform = CGAffineTransformMakeTranslation(0, 0)
            }, completion: nil)
    }

    var pageVc:YixiPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setttingUpTable()
        self.tableView.registerNib(UINib(nibName: "TalkTableViewCell", bundle: nil), forCellReuseIdentifier: "TalkTableViewCell")
        pageVc = self.storyboard?.instantiateViewControllerWithIdentifier("YixiPageViewController") as YixiPageViewController
        pageVc.delegate = self
        self.addChildViewController(pageVc)
        self.tableView.tableHeaderView = pageVc.view
        pageVc.didMoveToParentViewController(self)
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        pageVc.view.frame = CGRectMake(0, 0, screenSize.width, 200)
        pageVc.headerSize = CGSize(width: screenSize.width, height: 200)
        self.view.setNeedsDisplay()
        var titleImage:UIImage = UIImage(named: "titleBarImage")!
        self.navigationItem.titleView = UIImageView(image: titleImage)
        let rightItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "category"), style: UIBarButtonItemStyle.Plain, target: self, action:Selector("categoryButtonTouched:"))
        rightItem.tintColor = UIColor.grayColor()
        self.navigationItem.rightBarButtonItem = rightItem
        var cache:NSDictionary? = self.getCache()
        if !(cache == nil) {
            self.fillData(cache!)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("appActive:"), name: UIApplicationDidBecomeActiveNotification, object: nil)
        reqInfo()
        self.categoryView.registerNib(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCollectionViewCell")
        var flow:UICollectionViewFlowLayout = self.categoryView.collectionViewLayout as UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsetsMake(15, 10, 15, 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return talksArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return TalkTableViewCell.cellForTableView(self.tableView, forInfo: talksArray[indexPath.row])
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedInfo = self.talksArray[indexPath.row]
        self.performSegueWithIdentifier("homeToDetail", sender: self)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    
    func pageViewPageDidClicked(index: Int) {
        self.selectedInfo = self.featuredArray[index]
        self.performSegueWithIdentifier("homeToDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if(segue.destinationViewController.isKindOfClass(DetailViewController)){
            var target = segue.destinationViewController as DetailViewController;
            target.info = self.selectedInfo as? Dictionary<String, AnyObject>
        }else if(segue.destinationViewController.isKindOfClass(TalkListTableViewController)){
            var target = segue.destinationViewController as TalkListTableViewController;
            println(self.selectedAlbum)
            target.albumInfo = self.selectedAlbum as Dictionary<String, AnyObject>
        }
    }
    
    func setttingUpTable(){
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    
    func cachePath()-> NSString{
        let homePath:NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
        let homepagePath = homePath + "/homepage.json"
        println(homepagePath)
        return homepagePath
    }
    
    func getCache()->NSDictionary?{
        var archivedData:NSData? = NSData(contentsOfFile: self.cachePath())
        if (archivedData == nil){
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObjectWithData(archivedData!) as NSDictionary!
    }
    
    func writeToCache(data:NSDictionary){
        var err: NSError?
        var archivedData = NSKeyedArchiver.archivedDataWithRootObject(data)
        var result = archivedData.writeToFile(self.cachePath(), atomically: true)
        println(result)
        
    }
    
    func fillData(data:NSDictionary){
        self.pageVc.talkArray = data["featured"] as NSArray
        self.featuredArray = data["featured"] as NSArray
        self.talksArray = data["latest"] as NSArray
        self.albumsArray = data["albums"] as NSArray
        self.tableView.reloadData()
    }
    
    func reqInfo(){
        var manager = AFHTTPRequestOperationManager();
        manager.GET("http://yixi.meow.fm/homepage.json", parameters: nil, success: { (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
        var myData:NSDictionary = data as NSDictionary
        self.fillData(myData)
        self.writeToCache(myData)
        println(data)
        }) { (operaton:AFHTTPRequestOperation!, error:NSError!) -> Void in
        println("error: \(error)")
        }
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageVc.view.frame = CGRectMake(0, 0, self.view.width(), 200)
        pageVc.headerSize = CGSize(width: self.view.width(), height: 200)
        self.setCategoryHeight()
    }
    
    
    func setCategoryHeight(){
        let screenWidth:CGFloat = UIScreen.mainScreen().bounds.width
        // cell size 85x34
        // inset  UIEdgeInsetsMake(5, 10, 5, 10)
        var numEachLine = Int((screenWidth-15)/90)
        var allNum:Int = albumsArray.count
        var line = allNum/numEachLine
        var allLines:Int = ((allNum%numEachLine)==0) ? line : line+1;
        var height:Int = allLines*39 + 25
        self.categoryView.setHeight(CGFloat(height))
    }
    
    override func viewDidAppear(animated: Bool) {
        print("*****")
        print(pageVc.view.frame);
        print(pageVc.scrollView.frame)
    }
    
    func appActive(noti:NSNotification){
        reqInfo()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        var cell: CategoryCollectionViewCell! = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryCollectionViewCell", forIndexPath: indexPath) as CategoryCollectionViewCell
        cell.albumInfo = self.albumsArray[indexPath.item] as Dictionary<String, AnyObject>
        return cell
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.albumsArray.count
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedAlbum = self.albumsArray[indexPath.item] as Dictionary<String, AnyObject>
        self.performSegueWithIdentifier("homeToList", sender: self)
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        var cell:UICollectionViewCell = self.categoryView.cellForItemAtIndexPath(indexPath)!
        cell.backgroundColor = UIColor(red: 34/255.0, green: 44/255.0, blue: 44/255.0, alpha: 0.3)
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        var cell:UICollectionViewCell = self.categoryView.cellForItemAtIndexPath(indexPath)!
        cell.backgroundColor = UIColor(red: 34/255.0, green: 44/255.0, blue: 44/255.0, alpha: 0.02)
    }
}

