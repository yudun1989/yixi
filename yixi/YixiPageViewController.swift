//
//  YixiPageViewController.swift
//  yixi
//
//  Created by yudun1989 on 8/13/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit



protocol YixiPageViewControllerDelegate {
    func pageViewPageDidClicked(index:Int)
}

class YixiPageViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    var imageViewArray:NSMutableArray = []
    var titleViewArray:NSMutableArray = []

    var delegate:YixiPageViewControllerDelegate?
    var headerSize:CGSize = CGSizeZero{
        didSet{
            self.scrollView.frame = CGRect(x: self.scrollView.frame.origin.x, y: self.scrollView.frame.origin.y, width: headerSize.width, height: headerSize.height)
        }
    }
    var talkArray:NSArray = []{
        didSet{
            for v  in imageViewArray{
                v.removeFromSuperview()
            }
            for v  in titleViewArray{
                v.removeFromSuperview()
            }
            imageViewArray.removeAllObjects()
            titleViewArray.removeAllObjects()
            
            var pageCount = talkArray.count
            for var i=0; i<pageCount; i++ {
                var imgBeforeEscape:String = talkArray[i]["cover_image_url"] as String
                var imgURL:String = imgBeforeEscape.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                var imgView:UIImageView=UIImageView()
                var viewWidth = Int(headerSize.width)*i
                imgView.frame = CGRect(origin: CGPoint(x: viewWidth,y: 0),size: CGSize(width: headerSize.width, height: headerSize.height))
                imgView.sd_setImageWithURL(NSURL(string: imgURL), placeholderImage: UIImage(named: "placeholder1"))
                imgView.contentMode = UIViewContentMode.ScaleAspectFill
                imgView.clipsToBounds = true
                imgView.userInteractionEnabled = true
                imgView.tag = i
                imageViewArray.addObject(imgView)
                var tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imagePressed:")
                tap.numberOfTapsRequired = 1
                tap.numberOfTouchesRequired = 1
                imgView.addGestureRecognizer(tap)
                
                var titleView:UIView = UIView(frame: CGRect(origin: CGPoint(x:viewWidth, y:140), size: CGSize(width: Int(headerSize.width), height: Int(headerSize.height)-140-10)))
                
                titleView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
                titleView.userInteractionEnabled = false
                var titleLabel:UILabel = UILabel(frame: CGRectMake(20, 0, titleView.width()-40, titleView.height()))
                titleLabel.text = talkArray[i]["name"] as String!
                titleLabel.textColor = UIColor.whiteColor()
                titleLabel.font = UIFont.systemFontOfSize(17)
                titleLabel.userInteractionEnabled = false
                titleView.addSubview(titleLabel)
                titleViewArray.addObject(titleView)
                scrollView.addSubview(imgView)
                scrollView.addSubview(titleView)
            }
            scrollView.contentSize = CGSizeMake(CGFloat(pageCount)*headerSize.width, headerSize.height)
            self.view.setNeedsLayout()
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        configScrollView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func configScrollView(){
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.scrollEnabled = true
        scrollView.pagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.delegate = self
    }

    
    func scrollViewDidScroll(scrollView: UIScrollView!) {
        var pageWidth:CGFloat = scrollView.frame.size.width
        var offX:CGFloat = scrollView.contentOffset.x
        var page = Int(offX/pageWidth)
        pageControl.currentPage=page
    }

    
    func imagePressed (tap:UITapGestureRecognizer){
        delegate?.pageViewPageDidClicked(tap.view!.tag)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
