//
//  CategoryCollectionViewCell.swift
//  yixi
//
//  Created by yudun1989 on 10/19/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit


class CategoryCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var nameLabel:UILabel!
    
    var albumInfo:Dictionary<String, AnyObject>!{
        didSet{
            nameLabel.text = albumInfo["name"] as NSString!
        }
    }    
}
