//
//  AppDelegate.swift
//  yixi
//
//  Created by yudun1989 on 8/10/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?


    func application(application: UIApplication!, didFinishLaunchingWithOptions launchOptions: NSDictionary!) -> Bool {
        // Override point for customization after application launch.
        
//        UIApplication.sharedApplication().registerForRemoteNotificationTypes(UIRemoteNotificationType.Badge | UIRemoteNotificationType.Alert | UIRemoteNotificationType.Sound)
//        var remoteNotification:AnyObject? = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]
        WeiboSDK.registerApp("1345202452")
        WXApi.registerApp("wx252fae17d1236872")

//        if let noti: NSDictionary = remoteNotification as? NSDictionary{
//            let objectType = noti["type"] as NSString
//            let objectId = noti["id"] as NSString
//            if objectType == "talk" {
//                var mainController:ViewController = self.window?.rootViewController as ViewController
//                mainController.tableView.backgroundColor = UIColor.yellowColor()
//            }
//        }
//        self.clearNotifications()
        
//        [Flurry setCrashReportingEnabled:YES];
//        
//        // Replace YOUR_API_KEY with the api key in the downloaded package
//        [Flurry startSession:@"YOUR_API_KEY"];
        return true
    }

    
    override init() {
        super.init()
        iRate.sharedInstance().daysUntilPrompt = 3
        iRate.sharedInstance().usesUntilPrompt = 5
    }
    
    func applicationWillResignActive(application: UIApplication!) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication!) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication!) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication!) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.clearNotifications()
    }

    func applicationWillTerminate(application: UIApplication!) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        var tokenDesc = deviceToken.description as NSString
        var trimmedToken = tokenDesc.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>")) as NSString
        var token = trimmedToken.stringByReplacingOccurrencesOfString(" ", withString: "")
        //        [self sendProviderDeviceToken:devTokenBytes];
    //发送到服务器
        
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
    }
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        self.clearNotifications()
        var result = userInfo as Dictionary
        println(result)
        println(result["type"])
        println(result["id"])

    }
    
    func clearNotifications(){
        let app:UIApplication = UIApplication.sharedApplication()
        app.applicationIconBadgeNumber = 0;
        app.cancelAllLocalNotifications()
    }
}

