//
//  YixiNavigationController.swift
//  yixi
//
//  Created by yudun1989 on 8/24/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import Foundation


class YixiNavigationController: UINavigationController {
    override func shouldAutorotate() -> Bool {
        return false
    }
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
    }
}
