//
//  Youku.swift
//  yixi
//
//  Created by yudun1989 on 8/18/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import Foundation

class Youku: NSObject {
    class func findVideoIdFromUrl(url:NSString) ->NSString? {
        var result:NSString?
        let patterns = ["http://v.youku.com/v_show/id_([\\w=]+).html", "^http://player.youku.com/player.php/sid/([\\w=]+)/v.swf", "^loader\\.swf\\?VideoIDS=([\\w=]+)", "^([\\w=]+)$"]
        for p in patterns {
            result = findStringByPatternInText(p, text: url)
        }
        return result
    }
    
    
    class func findStringByPatternInText(pattern:String, text:String) ->String?{
        var result:String?
        var re = Regex(pattern)
        var res = re.matches(text)
        if res.count > 0 {
            var checkingresult:NSTextCheckingResult = res.firstObject as NSTextCheckingResult
            var range = checkingresult.rangeAtIndex(1)
            result = (text as NSString).substringWithRange(range)
        }
        return result
    }
    
    class func getFullUrl(url:String) -> String?{
        var result:String?
        var videoId = self.findVideoIdFromUrl(url)
        if (videoId != nil){
            result = "http://v.youku.com/v_show/id_\(videoId).html"
        }
        return result
    }
    
    
    class func findVideoId2FromVideoHtmlString(htmlString:String) -> String{
        let pattern = "var\\s+videoId2\\s*=\\s*'(\\S+)'"
        var videoId2:String? =  findStringByPatternInText(pattern, text: htmlString);
        return videoId2!
    }
    


    class func getTextWithUrl(url:String, function:((String) -> Void)){
        var request:NSURLRequest = NSURLRequest(URL: NSURL(string: url)!)
        var operation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: request)
        operation.setCompletionBlockWithSuccess({ (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
            function(operation.responseString)
            }, failure: { (operation:AFHTTPRequestOperation!, error:NSError!) -> Void in
                println(error)
        })
        operation.start()
        
    }
    
    class func getJsonDataByVideoId2(videoID2:String, success:(NSDictionary->Void), failure:(NSError->Void)){
        var url = "http://v.youku.com/player/getPlayList/VideoIDS/\(videoID2)"
        var request:NSURLRequest = NSURLRequest(URL: NSURL(string: url)!)
        var operation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: request)
        operation.setCompletionBlockWithSuccess({ (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
                var jsonErr:NSError? = nil
                var json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data as NSData, options: NSJSONReadingOptions.allZeros, error: &jsonErr);
                success(json as NSDictionary)
            }, failure: { (operation:AFHTTPRequestOperation!, error:NSError!) -> Void in
                println(error)
        })
        operation.start()
    }
    
    class func findVideoByVideoInfo(dict:AnyObject, vType:NSString){
        let dataArray = dict["data"] as NSArray
        let dataDic = dataArray[0] as NSDictionary
        let segs = dataDic["segs"] as NSDictionary
        let types = dataDic["streamtypes"] as NSArray
        if !types.containsObject(vType){
            return
        }
        var seed: Int = dataDic["seed"] as Int
        var sourceString:Array = Array("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/\\:._-1234567890")

        let fileType = ["hd3":"flv", "hd2":"flv", "mp4":"mp4", "flv":"flv"][vType]
        var mixed:Array = Array<String>()
        while(sourceString.count>0){
            seed = (seed * 211 + 30031) & 0xFFFF
            var index = (seed * (sourceString.count)) >> 16
            var c:Character = sourceString[index] as Character
            sourceString.removeAtIndex(index)
            mixed.append("\(c)")
        }
        
        let idsArrayIds:NSDictionary = dataDic["streamfileids"] as NSDictionary
        let idsString:String = idsArrayIds[vType] as String
        let idsArray:Array = idsString.componentsSeparatedByString("*")
        let ids = idsArray[0...idsArray.count-2]
        var vid:String = ""
        for i in ids{
            var index = i.toInt()
            vid += "\(mixed[index!])"
        }
        var timeInterval = NSDate().timeIntervalSince1970*1000
        

//        var sid = [NSString stringWithFormat:@"%ld%d%d", (long)longtimeInterval, arc4random()%1000+1000, arc4random()%8000+1000];
        var sid = "\(Int(timeInterval))\(arc4random()%1000+1000)\(arc4random()%8000+1000)"
        var urls = []
        var streamSegs:NSArray = segs[vType] as NSArray
        for seg in streamSegs{
            println(seg)
            
            var noNum:String = seg["no"] as String
            
            var no:NSMutableString = NSMutableString()
            no.appendFormat("%02x", noNum)
            var k = seg["k"] as String!
            var seconds = seg["seconds"] as Int!
            var noUpper = no.uppercaseString
            var vid2 = vid as NSString
            var firstVidString = vid2.substringWithRange(NSRange(location: 0,length: 8))
            var secondVidString = vid2.substringWithRange(NSRange(location: 0,length: 10))
            var url = "http://f.youku.com/player/getFlvPath/sid/\(sid)_\(no)/st/\(vType)/fileid/\(firstVidString)\(noUpper)\(secondVidString)?K=\(k)&ts=\(seconds)"
            println(url)
        }
    }
}
