//
//  TalkListTableViewController.swift
//  yixi
//
//  Created by yudun1989 on 8/14/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit


class TalkListTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var albumInfo:Dictionary<String, AnyObject>!
    var albumId:NSNumber!
    var talksArray:NSMutableArray = NSMutableArray()
    var selectedInfo:AnyObject!
    @IBOutlet var tableView: TMOTableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.albumInfo["name"] as NSString
        self.albumId = self.albumInfo["id"] as NSNumber
        self.tableView.registerNib(UINib(nibName: "TalkTableViewCell", bundle: nil), forCellReuseIdentifier: "TalkTableViewCell")
//        self.setupFirstLoad()//When set up finished, it will execute Immediately.
        self.setupLoadMore()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source


    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return talksArray.count;
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return TalkTableViewCell.cellForTableView(self.tableView, forInfo: talksArray[indexPath.row])
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedInfo = self.talksArray[indexPath.row]
        self.performSegueWithIdentifier("listToDetail", sender: nil)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.destinationViewController.isKindOfClass(DetailViewController)){
            var target = segue.destinationViewController as DetailViewController;
            target.info = self.selectedInfo as? Dictionary<String, AnyObject>
        }
    }
    
    func setupFirstLoad(){
        self.tableView.firstLoadWithBlock({ (tableView:TMOTableView!, data:AnyObject!) -> Void in
            var manager = AFHTTPRequestOperationManager();
            manager.GET("http://yixi.meow.fm/album/\(self.albumId)/talks.json", parameters: ["limit":20, "offset":0], success: { (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
                    let dict = data as Dictionary<String,AnyObject>
                    println(dict);
                    self.talksArray.addObjectsFromArray(dict["talks"] as NSArray)
                    self.tableView.reloadData()
                    self.tableView.myFirstLoadControl.done()
                }) { (operation:AFHTTPRequestOperation!, error:NSError!) -> Void in
                    self.tableView.myFirstLoadControl.fail()
            }
            }, withLoadingView: nil, withFailView: nil)
        self.tableView.myFirstLoadControl.allowRetry = true
    }
    
    func setupLoadMore(){
        self.tableView.loadMoreWithCallback({ (table:TMOTableView!, data:AnyObject!) -> Void in
            var manager = AFHTTPRequestOperationManager();
            manager.GET("http://yixi.meow.fm/album/\(self.albumId)/talks.json", parameters: ["limit":20, "offset":self.talksArray.count], success: { (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
                let dict = data as Dictionary<String,AnyObject>
                var moreTalks = dict["talks"] as NSArray
                if(moreTalks.count>0){
                    var originIndex = self.talksArray.count
                    var indexes:Array = Array<NSIndexPath>()
                    self.talksArray.addObjectsFromArray(moreTalks)
                    for var i = originIndex; i < self.talksArray.count; ++i {
                        indexes.append(NSIndexPath(forRow: i, inSection: 0))
                    }
                    self.tableView.insertRowsAtIndexPaths(indexes, withRowAnimation: UITableViewRowAnimation.Left)
                    self.tableView.myLoadMoreControl.done()
                }else{
                    self.tableView.myLoadMoreControl.invalid(true, hide:true)
                }
                }) { (operation:AFHTTPRequestOperation!, error:NSError!) -> Void in
                    self.tableView.myLoadMoreControl.fail()
            }
        }, withDelay: 0.0)
    }
}
