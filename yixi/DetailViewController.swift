//
//  DetailViewController.swift
//  yixi
//
//  Created by yudun1989 on 8/11/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit
import AVKit


class DetailViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var paragraphLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var videoPlayButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var speakerNameLabel: UILabel!
    
    @IBOutlet weak var wechatButton: UIButton!
    @IBOutlet weak var weiboButton: UIButton!
    @IBOutlet weak var momentsButton: UIButton!

    var info:Dictionary<String, AnyObject>?;
    var talkId:NSString?
    var moviePlayer:MPMoviePlayerViewController?;
    var movieView:UIView?;
    @IBAction func playButtonTouched(sender: AnyObject) {
        self.videoPlayButton.hidden = true
        self.indicator.startAnimating()
        if let myinfo = self.info?{
            if let m3u8:String? = self.info!["m3u8"] as? String{
                if (m3u8 == nil){
                    self.webView.loadRequest(NSURLRequest(URL: NSURL(string: myinfo["video"] as NSString)!))
                }else{
                    self.moviePlayer = MPMoviePlayerViewController(contentURL: NSURL(string: m3u8!));
                    self.presentMoviePlayerViewControllerAnimated(self.moviePlayer!)
                    self.moviePlayer?.moviePlayer.play()
                }
            }else{
                self.webView.loadRequest(NSURLRequest(URL: NSURL(string: myinfo["video"] as NSString)!))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = info!["name"] as NSString
        self.webView.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("moviePlayerStateChanged:"), name:
            UIWindowDidBecomeHiddenNotification , object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("mpMoviePlayerPlayFinished:"), name:
            UIWindowDidBecomeVisibleNotification , object: nil);
        let rightItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"), style: UIBarButtonItemStyle.Plain, target: self, action:Selector("shareButtonTouched:"))
        self.navigationItem.rightBarButtonItem = rightItem

        if self.info != nil {
            self.inflateTalk()
        }else if self.talkId != nil {
            var manager = AFHTTPRequestOperationManager();
            manager.GET("http://yixi.meow.fm/talk/\(self.talkId)/show.json", parameters: nil, success: { (operation:AFHTTPRequestOperation!, data:AnyObject!) -> Void in
                    self.info = data as? Dictionary
                    self.inflateTalk()
                }) { (operaton:AFHTTPRequestOperation!, error:NSError!) -> Void in
                    println("error: \(error)")
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.paragraphLabel.y() + self.paragraphLabel.height() + 30 + 40 + 20)
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func relatedTalkButtonTouched(sender: AnyObject){
        
    }
    
    func webView(webView: UIWebView!, shouldStartLoadWithRequest request: NSURLRequest!, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
//    myVid.autoplay=true;
    func webViewDidFinishLoad(webView: UIWebView!) {
        var js = "myVid=document.getElementsByTagName(\"video\")[0];myVid.play();"
        webView.stringByEvaluatingJavaScriptFromString(js)
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.indicator.stopAnimating()
        self.videoPlayButton.hidden = false
    }
    
    override func viewDidAppear (animated: Bool) {
        var js = "myVid=document.getElementsByTagName(\"video\")[0];myVid.stop()"
        webView.stringByEvaluatingJavaScriptFromString(js)
        self.indicator.stopAnimating()
        self.videoPlayButton.hidden = false
    }
    
    func inflateTalk(){
        if self.info == nil{
            return;
        }
        var attText = NSMutableAttributedString(string: self.info!["simple_description"] as NSString)
        var paragrahStyle = NSMutableParagraphStyle()
        paragrahStyle.lineSpacing = 6
        attText.addAttribute(NSParagraphStyleAttributeName, value: paragrahStyle, range: NSMakeRange(0, attText.length))
        var title = self.info!["name"] as NSString
        self.titleLabel.text = "\(title)"
        self.speakerNameLabel.text = self.info!["speaker"] as NSString
        self.paragraphLabel.attributedText = attText;
        self.paragraphLabel.sizeToFit()
        var imgBeforeEscape = self.info!["image_url"] as NSString
        var imgURL:String = imgBeforeEscape.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        self.coverImage.sd_setImageWithURL(NSURL(string: imgURL), placeholderImage: UIImage(named: "placeholder1"))
        self.scrollView.contentSize = CGSize(width: self.scrollView.width(), height: self.paragraphLabel.y()+self.paragraphLabel.height()+30)
    }
    
    
    func moviePlayerStateChanged(noti:NSNotification){
        self.indicator.stopAnimating()
        self.videoPlayButton.hidden = false
    }
    
    func mpMoviePlayerPlayFinished(noti:NSNotification){
        self.indicator.stopAnimating()
        self.videoPlayButton.hidden = false
    }
    
    @IBAction func shareButtonTouched(sender: AnyObject) {
        var name:String = self.info!["name"] as NSString
        var string = "「\(name)」，分享自一席App: https://itunes.apple.com/us/app/yi-xi/id912814691?ls=1&mt=8"
        var urlString:String = self.info!["url"] as NSString
        var url:NSURL = NSURL(string: urlString)!
        var activity:UIActivityViewController = UIActivityViewController(activityItems: [string, url], applicationActivities: nil)
        self.navigationController?.presentViewController(activity, animated: true, completion: nil)
    }
    
    
    @IBAction func shareToWeiboButtonTouched(sender: AnyObject) {
        var name:String = self.info!["name"] as NSString
        var urlString:String = self.info!["url"] as NSString
        var videoURL:String = self.info!["video"] as NSString
        var string = "「\(name)」，视频地址:\(videoURL)，分享自一席App: https://itunes.apple.com/us/app/yi-xi/id912814691?ls=1&mt=8 "
        var url:NSURL = NSURL(string: urlString)!
        var message:WBMessageObject = WBMessageObject()
        message.text = string
        var webObject:WBWebpageObject = WBWebpageObject()
        var myId:Int = self.info!["id"] as Int
        webObject.objectID = "\(myId)"
        webObject.title = name
        webObject.webpageUrl = videoURL
        webObject.description = string
        message.mediaObject = webObject
        var request:WBSendMessageToWeiboRequest = WBSendMessageToWeiboRequest.requestWithMessage(message) as WBSendMessageToWeiboRequest
        WeiboSDK.sendRequest(request)
    }

    @IBAction func shareToWechatButtonTouched(sender: AnyObject) {
        if(!WXApi.isWXAppInstalled()){
            var alert:UIAlertView = UIAlertView(title: "此功能不可使用", message: "微信未安装", delegate: self, cancelButtonTitle: "确定")
            alert.show()
        }
        var name:String = self.info!["name"] as NSString
        var string = "「\(name)」，分享自一席App: https://itunes.apple.com/us/app/yi-xi/id912814691?ls=1&mt=8"
        var urlString:String = self.info!["url"] as NSString
        var videoURL:String = self.info!["video"] as NSString
        var url:NSURL = NSURL(string: urlString)!
        var desc:String = "分享自一席App"
        var message:WXMediaMessage = WXMediaMessage()
        message.title = name
        message.description = desc
        message.setThumbImage(UIImage(named: "shareIcon.png"))
        var ext:WXWebpageObject = WXWebpageObject()
        ext.webpageUrl = videoURL
        
        message.mediaObject = ext;
        
        var req:SendMessageToWXReq = SendMessageToWXReq()
        req.bText = false
        req.message = message;
        req.scene = Int32(WXSceneSession.value)
        WXApi.sendReq(req)
    }

    @IBAction func shareToMomentsButtonTouched(sender: AnyObject) {
        if(!WXApi.isWXAppInstalled()){
            var alert:UIAlertView = UIAlertView(title: "此功能不可使用", message: "微信未安装", delegate: self, cancelButtonTitle: "确定")
            alert.show()
        }
        var name:String = self.info!["name"] as NSString
        var string = "「\(name)」，分享自一席App: https://itunes.apple.com/us/app/yi-xi/id912814691?ls=1&mt=8"
        var urlString:String = self.info!["url"] as NSString
        var videoURL:String = self.info!["video"] as NSString
        var url:NSURL = NSURL(string: urlString)!
        var desc:String = "分享自一席App"
        var message:WXMediaMessage = WXMediaMessage()
        message.title = name
        message.description = desc
        message.setThumbImage(UIImage(named: "shareIcon.png"))
        var ext:WXWebpageObject = WXWebpageObject()
        ext.webpageUrl = videoURL
        message.mediaObject = ext;
        
        var req:SendMessageToWXReq = SendMessageToWXReq()
        req.bText = false
        req.message = message;
        req.scene = Int32(WXSceneTimeline.value)
        WXApi.sendReq(req)
    }
    
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue);
    }

    override func shouldAutorotate() -> Bool {
        return true
    }
    
}
