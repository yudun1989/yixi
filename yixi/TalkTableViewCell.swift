//
//  TalkTableViewCell.swift
//  yixi
//
//  Created by yudun1989 on 8/10/14.
//  Copyright (c) 2014 meow.fm. All rights reserved.
//

import UIKit

class TalkTableViewCell: UITableViewCell {
    @IBOutlet weak var talkTitleLabel: UILabel!
    @IBOutlet weak var talkCoverImageView: UIImageView!
    @IBOutlet weak var talkContentLabel: UILabel!
    
    var info:AnyObject!
    
    class func cellForTableView(tableView:UITableView, forInfo info:AnyObject) -> UITableViewCell{
        // type method implementation goes here
        var cell:TalkTableViewCell? = tableView.dequeueReusableCellWithIdentifier("TalkTableViewCell") as? TalkTableViewCell
        cell!.info = info;
        //we know that cell is not empty now so we use ! to force unwrapping
        cell!.talkCoverImageView.image = nil;
        var imgBeforeEscape = info["image_url"] as NSString
        var imgURL:String = imgBeforeEscape.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
//        var currentURL = cell!.talkCoverImageView.imageURL
//        AsyncImageLoader.sharedLoader().cancelLoadingURL(currentURL)
//        cell!.talkCoverImageView.imageURL = nil;
        cell!.talkCoverImageView.sd_setImageWithURL(NSURL(string: imgURL), placeholderImage: UIImage(named: "placeholder1"))
        var speaker = info["speaker"] as NSString
        var name = info["name"] as NSString
        if speaker == ""{
            cell!.talkTitleLabel.text = "\(name)"
        }else{
            cell!.talkTitleLabel.text = "\(speaker)：\(name)"
        }
        cell!.talkContentLabel.text = info["simple_description"] as NSString
        return cell!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
