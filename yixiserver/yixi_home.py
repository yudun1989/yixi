#coding:utf-8

import requests
import bs4
import re
from model import Talk, Album, q
from crawl_yixi_item import crawl_item

def crawl():
	res = requests.get("http://yixi.tv/")
	if res.status_code !=200:
		return 
	res_text = res.text
	soup = bs4.BeautifulSoup(res_text)
	featured = soup.find_all(name='article')
	for featured_article in featured:
		a_tag = featured_article.find('a')
		video_cover = featured_article["style"]
		if not a_tag:
			continue
		video_url = a_tag["href"]
		video_cover_url = re.search("'(.*?)'", video_cover)
		if not video_cover_url:
			continue
		video_cover_url = video_cover_url.group(1)
		video = Talk.get_by_talkurl(video_url)
		if not video:
			q.enqueue(crawl_item, args=('', video_url))
			continue
		video.cover_image_url = video_cover_url
		video.save()

			


if __name__ == '__main__':
	crawl()