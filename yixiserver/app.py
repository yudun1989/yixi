#coding:utf-8

from flask import Flask, render_template, request, Response
from model import Talk, Album
import json
import os

app = Flask(__name__, static_url_path='/static')

@app.route("/")
def home():
    return render_template('index.html')



@app.route("/homepage.json")
def api_home():
    albums = []
    # albums.append({'id':0, 'name':'全部'})
    for album in Album.select():
        albums.append({'id':album.id, 'name':album.name})
    # albums.append({'id':len(albums), 'name':'最新'})
    featured =[]
    # for talk in Talk.select().where((Talk.url is not None) | (Talk.cover_image_url is not None)).limit(3):
    for talk in Talk.raw('select * from talk where (url is not null and cover_image_url is not null) order by id desc limit 3'):
        featured.append(dump_talk(talk))
    latest = []
    # for talk in Talk.select().where((Talk.url is not None) | (Talk.cover_image_url is None)).limit(10):
    for talk in Talk.raw('select * from talk where (url is not null and cover_image_url is null) order by id desc limit 10'):
        latest.append(dump_talk(talk))
    result = {'albums':albums,'featured':featured,'latest':latest}
    final = json.dumps(result)
    return Response(final, content_type='text/json; charset=utf-8')


@app.route("/album/<int:album_id>/talks.json")
def api_album(album_id):
    limit = request.args.get('limit', '10')
    offset = request.args.get('offset', '0')
    limit = int(limit)
    offset = int(offset)
    talks = []
    album_count = len(list(Album.select()))
    if album_id == 0:
        for talk in Talk.select().order_by(Talk.id.desc()).limit(limit).offset(offset):
            talks.append(dump_talk(talk))
    elif album_id == album_count+1:
        for talk in Talk.select().where(Talk.album_id == 0).order_by(Talk.id.desc()).limit(limit).offset(offset):
            talks.append(dump_talk(talk))
    else:
        for talk in Talk.select().where(Talk.album_id == album_id).order_by(Talk.id.desc()).limit(limit).offset(offset):
            talks.append(dump_talk(talk))
    result = {'count':len(talks),'talks':talks}
    final = json.dumps(result)   
    return Response(final, content_type='text/json; charset=utf-8')


def dump_talk(talk):
    result = {
        'id':talk.id,
        'name':talk.name,
        'url':talk.url,
        'video':talk.video,
        'image_url': talk.image_url or talk.youku_image_url or talk.cover_image_url,
        'simple_description':talk.simple_description or talk.youku_desc,
        'cover_image_url':talk.cover_image_url,
        'speaker':talk.speaker or '',
        'm3u8':talk.m3u8,
        'forcejs': 1,
        'youku_image_url':talk.youku_image_url
    }
    return result


if __name__ == "__main__":
    app.run('0.0.0.0', 4000, False)