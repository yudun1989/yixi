#coding:utf-8

from model import Talk, Album
from youku_parser import parse_url
import requests
import bs4



def crawl_m3u8_item(url):
    talk = Talk.get_by_vurl(url)
    if not talk:
        return
    m3u8, logo, name = parse_url(url)
    res = requests.get(m3u8)
    if res.text == '#EXTM3U\r\n#EXT-X-TARGETDURATION:-1\r\n#EXT-X-VERSION:3\r\n#EXT-X-ENDLIST\r\n':
        talk.m3u8 = None
        talk.youku_image_url = logo
        talk.save()
        raise Exception("nonono")
    if not talk.name:
        talk.name = name
    talk.m3u8 = m3u8
    talk.youku_image_url = logo
    talk.save()
    #每次都更新


#优酷的视频简介内容，保存到youku_desc中
def crawl_youku_item(url):
    talk = Talk.get_by_vurl(url)
    if not talk:
        return;
    res = requests.get(url)
    if not res.status_code == 200:
        return
    soup = bs4.BeautifulSoup(res.text)
    desc = soup.find(id='text_long')
    if desc:
        text = desc.text
        text = text.rstrip(u'隐藏')
        talk.youku_desc = text
        talk.save()