from peewee import *
from rq import Queue
from redis import Redis


db = MySQLDatabase("yixi", host="localhost", user="root", passwd="yzdycz")

class BaseModel(Model):
    id = IntegerField()
    class Meta:
        database = db
    @classmethod
    def get_by_id(cls, item_id):
        result = None
        try:
            result = cls.get(cls.id == item_id)
        except Exception as e:
            result = None
        return result


class Talk(BaseModel):
    speaker = CharField()
    name = CharField()
    url = CharField()
    description = CharField()
    video = CharField()
    album_id = IntegerField()
    youku_desc = CharField()
    image_url = CharField()
    simple_description = CharField()
    cover_image_url = CharField()
    youku_image_url = CharField()
    m3u8 = CharField()
    @classmethod
    def get_by_vid(cls, vid):
        full_url = 'http://v.youku.com/v_show/id_'+vid+'.html'
        result =None 
        try:
            result = cls.get(Talk.video==full_url)
        except Exception as e:
            result = None
        return result

    @classmethod
    def get_by_vurl(cls, vurl):
        result =None 
        try:
            result = cls.get(Talk.video==vurl)
        except Exception as e:
            result = None
        return result

    @classmethod
    def get_by_talkurl(cls, talkurl):
        result =None 
        try:
            result = cls.get(Talk.url==talkurl)
        except Exception as e:
            result = None
        return result

class Album(BaseModel):
    name = CharField()
    url = CharField()
    @classmethod
    def get_by_url(cls, url):
        try:
            result = cls.get(Album.url==url)
        except Exception as e:
            result = None
        return result

db.connect()
q = Queue(connection=Redis())
