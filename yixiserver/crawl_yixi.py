#coding:utf-8
import requests
import bs4
from model import Talk, Album, q
from crawl_yixi_item import crawl_item

albums = [
		'http://yixi.tv/talks/',
		'http://yixi.tv/project-type/innovation/',
		'http://yixi.tv/project-type/planet/',
		'http://yixi.tv/project-type/deeds/',
		'http://yixi.tv/project-type/craftsmanship/',
		'http://yixi.tv/project-type/art/',
		'http://yixi.tv/project-type/being/',
		'http://yixi.tv/project-type/wall-of-life/',
		'http://yixi.tv/project-type/science/',
		'http://yixi.tv/project-type/views/',
		'http://yixi.tv/project-type/roaming/',
		'http://yixi.tv/project-type/design/',
		'http://yixi.tv/project-type/shang-ye/'
	]


def parse_url(base_url, index):
	url = base_url + 'page/' + str(index)
	res = requests.get(url)
	if not res.status_code == 200:
		return
	soup = bs4.BeautifulSoup(res.text)
	talks = soup.find_all(attrs={"class": "talks-item"})
	for talk in talks:
		pic_url = talk.find('img')
		video_url = talk.find_all('a')
		if pic_url and len(video_url)>1:
			cover_url = pic_url['src']
			talk_url = video_url[1]['href']
			print cover_url, talk_url
			q.enqueue(crawl_item, args=(cover_url, talk_url))


	if len(talks)>0:
		parse_url(base_url, index+1)


for album in albums:
	parse_url(album, 0)