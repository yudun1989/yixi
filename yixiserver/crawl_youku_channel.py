#coding:utf-8
import requests
import bs4
from model import Talk, Album, q
from job_processor import crawl_m3u8_item, crawl_youku_item

#爬取每个视频的信息,爬完分别放两个任务，一个是，爬他们的youku_desc,另一个是爬m3u8
#本脚本有一定几率失败

def crawl_page(index, retry):
    url = 'http://www.youku.com/show_episode/id_z2c9b63e691e611e2b356.html?dt=json&divid=reload_'+str(index)+'&__rt=1&__ro=reload_21';
    headers = {'UserAgent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36'}
    res = requests.get(url, headers=headers)
    if not res.status_code == 200:
        if(retry):
            crawl_page(index, False)
        return
    soup = bs4.BeautifulSoup(res.text)
    anchors = soup.select('.ititle_w')
    if not anchors:
        if(retry):
            crawl_page(index, False)
        return 
    for anchor in anchors:
        yixi_id = anchor.find('label').text
        url = anchor.find('a')['href']
        name = anchor.find('a')['title']
        talk = Talk.get_by_id(yixi_id)
        print yixi_id, name, talk, url
        if not talk:
            talk = Talk.create(id=yixi_id, video=url)
            talk.save()
        result1 = q.enqueue(crawl_m3u8_item, url)
        result2 = q.enqueue(crawl_youku_item, url)

    index = index+10;
    crawl_page(index, True)





if __name__ == '__main__':
    crawl_page(0, True)
    # crawl_youku_item('http://v.youku.com/v_show/id_XNDQ4Mjc4MjA4.html')

