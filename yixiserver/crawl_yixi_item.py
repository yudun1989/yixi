#coding:utf-8
import requests
import bs4
import re
from model import Talk, Album

def crawl_item(cover_url, talk_url):
	res = requests.get(talk_url)
	if not res.status_code == 200:
		return 
	soup = bs4.BeautifulSoup(res.text)
	inner = soup.find(id='sidebar-inner')
	desc_text = ''
	video_id = ''
	speaker = ''
	title = ''
	if inner:
		ptag = inner.find(name='p')
		if ptag:
			desc_text = ptag.text
	obj = soup.find('iframe')
	if obj:
		iframe_src = obj['src']
		video_id_matched = re.search(r'embed/(.*?)$',iframe_src)
		if video_id_matched:
			video_id = video_id_matched.group(1)
	if not video_id:
		return
	h1 = soup.find('h1')
	if not h1:
		return
	h1 = h1.text.encode('utf-8')
	result = h1.split('：')
	if len(result) == 1:
		result = result[0].split(':')
	if len(result)>1:
		speaker, title = result
	elif len(result)==1:
		title = result[0]

	# print title, speaker, video_id, desc_text
	talk = Talk.get_by_vid(video_id)
	if talk and not talk.simple_description:
		talk.name = title
		talk.speaker = speaker
		talk.simple_description = desc_text
		talk.image_url = cover_url
		talk.url = talk_url
		talk.save()



if __name__ == '__main__':
	crawl_item('http://yixi.tv/talk/tian-feng-xun-zhao-di-er-ge-di-qiu/')